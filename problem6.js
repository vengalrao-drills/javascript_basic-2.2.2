// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const inventory = require("./inventory/inventory"); // importing the data from the folder inventory-> from inventory.js file
let BMWAudi = []; // creating a BMWAudi array
function BMWAndAudi() {
  inventory.map((index) => {               // using the map function to iterate the inventory data
    const { car_model, car_make } = index; // destructuring  the data
    if (
      car_make.toLowerCase() == "BMW".toLowerCase() || car_make.toLowerCase() == "Audi".toLowerCase()
    ) {
      // if the car_make equals to BMW or Audi the push to the BMWAudi
      BMWAudi.push(car_model);    // push the car_model data to BMWAudi array
    }
  });
  return JSON.stringify(BMWAudi); // returing the BMWAudi array that has only cars made of BMW and Audi cars
}

module.exports = BMWAndAudi;      // exporting the BMWandAudi function to the other modules