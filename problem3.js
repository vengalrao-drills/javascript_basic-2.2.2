// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require("./inventory/inventory");   // importing the data from the folder inventory-> from inventory.js file
let carModel = [];                  // creating the array

function sortCars() {
  inventory.map((index) => {        // using the map function to iterate the inventory data
    carModel.push(index.car_model); // pushing the data into car_Model 
  });
  return carModel.sort();           // sorting the data
}

module.exports = sortCars;          // exporting the sortCars to the other modules