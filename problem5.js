// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require("./inventory/inventory");   // importing the data from the folder inventory-> from inventory.js file
let oldCars = [];

function olderCars() {
  inventory.map((index) => {                // using map function on inventory data
    const { car_year, car_model } = index;  // destructuring the data
    if (car_year < 2000) {                  // if the year is less than 2000 consition satisfies
      oldCars.push(car_model);              // pushing the data into the oldCars
    }
  });
  return `${oldCars} -- ${oldCars.length}`;  // return the data
}

module.exports = olderCars;  // exporting the olderCars to the other modules