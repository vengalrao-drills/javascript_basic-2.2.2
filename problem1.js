// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const inventory = require("./inventory/inventory"); // importing the data from inventory folder- from inventory.js

function findCar(number) {
  let carData = inventory.filter((index) => {  // using filter function. iterating through the data-inventory.js
    if (index.id == number) {       // if the data matches with your number return true else false
      return true;
    }
  });
  const { id, car_make, car_model, car_year } = carData[0];  // destructuring the data 

  return `Car ${id} is a ${car_make} ${car_model} ${car_year}`;  // returning the data into a specific format
}

module.exports = findCar;  // exporting the findCar function to the other modules.