// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const inventory = require("./inventory/inventory");  // importing the inventory data from the folder inventory-> from inventory.js file
let yearsData = [];                    //creating a array made of yearsData to store the years

function carYears() {
  inventory.map((index) => {           // using map on inventory data
    yearsData.push(index.car_year);    //  pushing the year to the yearsData
  });

  return yearsData;                    // returing the yearsData
}

module.exports = carYears;   // exporting the carYears to the other modules
