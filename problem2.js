// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

const inventory = require("./inventory/inventory"); // importing the data from inventory folder- from inventory.js
// here we need to send the last car. so we need to find the last index then return it.
function lastCar() {
  lastAdded = inventory.length - 1;                   // finding the last index.
  let { car_make, car_model } = inventory[lastAdded]; // destructing done
  return `Last car is a ${car_make} ${car_model}`;    // output is formmated and returned

  // return `Last car is a ${inventory[lastAdded].car_make} ${inventory[lastAdded].car_model}`;
}

module.exports = lastCar;   // exporting the lastCar function to other modules